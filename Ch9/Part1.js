var fs = require("fs"),
    path_to_file = process.argv[2];

fs.readFile(path_to_file, 'utf8',
    function(err, fiLoaded){
        fs.readFile('../stop_words.txt', 'utf8', 
        function(err1, fiStopWords){
            
            var TFTheOne = function(v){
                this._value = v;
            }

            TFTheOne.prototype = {
                bind : function(func) {
                    this._value = func(this._value);
                    return this;
                },
                printme : function() {
                    console.log(this._value);
                }
            }

            function read_file(path_to_file) {
                return fiLoaded;//we did this part in the opening (it is easier this way)
            }

            function filter_chars(str_data){
                return (str_data+"").split(/[\W_+]/);
            }

            function normalize(str_data){
                return (str_data+"").toLowerCase();
            }

            function scan(str_data){
                var scanned = [];
                for(var i=0; i<str_data.length; i++)
                    if (str_data[i].length > 1)
                        scanned.push(str_data[i]);
                return scanned;
            }

            function remove_stop_words(str_data){
                var stop_words = fiStopWords.split(',');
                var data = [];
                for(var i=0; i<str_data.length; ++i)
                    if(stop_words.indexOf(str_data[i]) == -1)
                        data.push(str_data[i]);
                return data;
            }

            function frequencies(word_list){
                console.log('counting . . .');
                var word_freqs = {};
                for(var i=0; i<word_list.length; ++i){
                    var w = word_list[i];
                    if(word_freqs.hasOwnProperty(w)) {
                        word_freqs[w]++;
                    }else
                        word_freqs[w] = 1;
                }
                return word_freqs;
            }

            function sort(word_freq){
                console.log('sorting . . .');
                var sorted = [];
                for(var i=0; i<Object.keys(word_freq).length; ++i) {
                    var key = Object.keys(word_freq)[i];
                    sorted.push({ key: key, value: word_freq[key] });
                }
                sorted.sort(function(a,b){ return b.value - a.value });
               return sorted;
            }

            function top25_freqs(word_freqs){
                var top25 = "";
                for(var i=0; i<25; ++i)
                    top25 += (word_freqs[i].key + "  -  " + word_freqs[i].value + "\n");
                return top25;
            }

            new TFTheOne(path_to_file)
                .bind(read_file)
                .bind(normalize)
                .bind(filter_chars)
                .bind(scan)
                .bind(remove_stop_words)
                .bind(frequencies)
                .bind(sort)
                .bind(top25_freqs)
                .printme();
            
        })
    })