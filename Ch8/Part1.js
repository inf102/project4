var fs = require("fs"),
    path_to_file = process.argv[2];

fs.readFile(path_to_file, 'utf8',
    function(err, fiLoaded){
        fs.readFile("../stop_words.txt", 'utf8',
        function(err1, data1){

            function read_file(path_to_file, func){
                console.log("read_file");
                var data = fiLoaded;
                func(data, normalize)
            }

            function normalize(str_data, func){
                console.log("normalize");
                func(str_data, remove_stop_words)
            }

            function remove_stop_words(word_list, func){
                console.log("remove_stop_words");
                var stop_words = data1.split(',');
                stop_words = stop_words.concat("a b c d e f g h i j k l m n o p q r s t u v w x y z".split(' '));
                var wl = [];
                for(var i=0; i<word_list.length; ++i)
                    if(word_list[i].length > 1
                        && stop_words.indexOf(word_list[i]) == -1)
                        wl.push(word_list[i]);
                func(wl, sort);
            }

            function sort(word_list, func){
                console.log("sort");
                var sorted = [];

                for(var i=0; i<Object.keys(word_list).length; ++i){
                    var key = Object.keys(word_list)[i];
                    sorted.push({key: key, value: word_list[key] });
                }

                sorted.sort(function(a,b){
                    return b.value - a.value;
                });
                func(sorted, no_op);
            }

            function filter_chars(str_data, func) {
                console.log("filter_chars");
                var p = (str_data+"").toLowerCase().split(/[\W_]+/);
                func(p, scan);
            }

            function scan(str_data, func){
                console.log("scan");
                func(str_data, frequencies);
            }

            function frequencies(word_list, func){
                console.log("frequencies");
                var wf = {};
                for(var i=0; i<word_list.length;++i){
                    var w = word_list[i];
                    if(wf.hasOwnProperty(w))
                        wf[w]++;
                    else
                        wf[w] = 1;
                }
                func(wf, print_text)
            }

            function print_text(word_freqs, func){
                console.log("print_text");
                for(var i=0; i<25; ++i){
                    console.log(
                        word_freqs[i].key
                        + "  -  " +
                        word_freqs[i].value);
                }
                func(null);
            }

            function no_op(func){
                console.log("no_op");
            }

            read_file(process.argv[1], filter_chars);
        })
    }
);

