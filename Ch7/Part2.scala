import scala.collection.mutable
import scala.io.Source

object Part2 {

  def main(args: Array[String]): Unit ={
    val wordslist = Source.fromFile(args(0))
      .getLines()
      .flatMap(_.split("[\\W_]+"))
      .toArray
    val stopwords = loadStopWord(Array[String](), first = true)
    val wordfreqs = collection.mutable.Map[String, Int]()

    println("Please wait, I'm counting...")
    count(wordslist, stopwords, wordfreqs)
    wf_print(wordfreqs)
  }

  def loadStopWord(items: Array[String], first: Boolean): Array[String] = {
    if(first){
      val arr = Source.fromFile("../stop_words.txt")
        .getLines()
        .flatMap(_.split("[\\W_]+"))
        .toArray
      loadStopWord(arr, false)
    } else items
  }

  /* *
    def loadStopWord(items: Array[String],
                     str: String,
                     first: Boolean): Array[String] = {
      if(first){
        val str = Source.fromFile("../stop_words.txt")
          .getLines()
          .flatMap(_)
        loadStopWord(Array[String](), str, false)
      } else items
    }
  * */

  def wf_print(wordfreqs: mutable.Map[String, Int]): Unit = {
    val results = wordfreqs.toSeq.sortBy(_._2).reverse.take(25)
    for((k, v) <- results) println(k + "  -  " + v)
  }

  def count(wordslist: Array[String],
            stopwords: Array[String],
            wordfreqs: mutable.Map[String, Int]): Unit = {
    if(wordslist.length == 1) {
      val word = wordslist(0)
      if(!stopwords.contains(word) && word.trim.length > 1){
        if(wordfreqs.keySet.contains(word))
          wordfreqs.update(word, wordfreqs(word)+1)
        else
          wordfreqs.put(word, 1)
      }
    } else if (wordslist.length > 1) {
      count(wordslist.take(1), stopwords, wordfreqs)
      count(wordslist.tail, stopwords, wordfreqs)
    }
  }

  def printAll(args: Array[String]): Unit ={
    if(args.length == 1){
      println(args(0))
    } else if (args.length > 1){
      printAll(args.take(1))
      printAll(args.tail)
    }

  }
}